'use strict';

/*****Guess My Number Game*****/

/* INITIALIZE VARIABLES */
let highScore = 0;
let secretNumber;
let score;
let hasWon;
let hasLost;

/* DECLARE FUNCTIONS */
const setNewSecretNumber = function () {
  secretNumber = Math.trunc(Math.random() * 20) + 1;
  return secretNumber;
};
const initGame = function () {
  setNewSecretNumber();
  score = 20;
  hasWon = false;
  hasLost = false;

  document.querySelector('.score').textContent = score;
  document.querySelector('.number').style.width = '15rem';
  document.querySelector('.number').textContent = '?';
  document.querySelector('body').style.backgroundColor = '#222';
  document.querySelector('.guess').value = '';
  document.querySelector('.message').textContent = 'Start guessing...';
};
const displayLossMessage = function () {
  document.querySelector('.message').textContent = '💥 You lost the game!';
};
const displayBlankMessage = function () {
  document.querySelector('.message').textContent = '⛔ No Number!';
};
const displayWinScreen = function () {
  // display secretNumber
  document.querySelector('.number').textContent = secretNumber;
  document.querySelector('.number').style.width = '25rem';

  // display victory message
  document.querySelector('.message').textContent = '🎉 Correct Number!';

  // display victory background
  document.querySelector('body').style.backgroundColor = '#60b347';
};
const displayInvalidMessage = function () {
  document.querySelector('.message').textContent = '⛔ Invalid Guess!';
};
const updateHighScore = function () {
  if (score > highScore) highScore = score;
  document.querySelector('.highscore').textContent = highScore;
};

/*AT RUNTIME*/
initGame();

// Event Handler for Check! button
// 1. if score is outside score range, set score to 0.
// 2. read guess from user input, parse to acceptable values (any number, '')
// 3. if game isn't won or lost yet, follow game logic & react to guess
// 4. if game has been either won or lost, do nothing.

document.querySelector('.check').addEventListener('click', function () {
  if (score < 0 || score > 20) score = 0;

  const guess =
    document.querySelector('.guess').value === ''
      ? ''
      : Number(document.querySelector('.guess').value);

  if (!hasWon && !hasLost) {
    switch (true) {
      case guess === '':
        displayBlankMessage();
        break;
      case guess < 1 || guess > 20:
        displayInvalidMessage();
        break;
      case guess === secretNumber:
        displayWinScreen();
        updateHighScore();
        hasWon = true;
        break;
      default:
        score--;
        document.querySelector('.score').textContent = score;
        if (score === 0) {
          hasLost = true;
          displayLossMessage();
        } else {
          guess > secretNumber
            ? (document.querySelector('.message').textContent = '📈 Too High!')
            : (document.querySelector('.message').textContent = '📉 Too Low!');
        }
    }
  } else if (hasWon || hasLost) {
  }
});

// Event Handler for Again! button
document.querySelector('.again').addEventListener('click', function () {
  initGame();
});
